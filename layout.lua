local CurrentPage = PageNames[props["page_index"].Value]
local gaColor = {
    Black = {0,0,0}, --Black
    White = {255,255,255}, --White
    Green = {178,255,51}, --Shure Green
    LtGray = {204,204,204} --Light Gray
}
local gsCat1 = "Unit Information~"
local gsCat2 = "Battery Information~"
if CurrentPage == "Control" then
  table.insert(graphics,{
    Type = "GroupBox",
    Fill = gaColor.Black,
    CornerRadius = 5,
    StrokeWidth = 1,
    Position = {5,5},
    Size = {550,370}
  })
  Logo = "--[[ #encode "logo.svg" ]]"
  table.insert(graphics,{
    Type = "Svg",
    Image = Logo,
    Position = {40,5},
    Size = {300,64},
    ZOrder = 1000
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "QLX-D",
    Position = {340, 5},
    Color = gaColor.Green,
    Size = {150,64},
    FontSize = 40,
    Font = "Open Sans",
    FontStyle = "Extrabold",
    HTextAlign = "Left"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "©",
    Position = {475, 20},
    Color = gaColor.Green,
    Size = {20,20},
    FontSize = 16,
    Font = "Open Sans",
    FontStyle = "Extrabold",
    HTextAlign = "Left"
  })
  table.insert(graphics,{
    Type = "Header",
    Text = "Information",
    Position = {15,70},
    Size = {530,11},
    Color = gaColor.Green,
    FontSize = 16,
    Font = "Roboto",
    FontStyle = "Bold",
    HTextAlign = "Center",
  })
  local gaLabels = {
    Pos = {X={20,20,20,20,20,20,20,20,20,20,20,20,20,400,380},Y={90,110,130,150,170,190,210,230,250,270,290,310,330,354,330}},
    Text = {"Unit Firmware Version:","Unit Mac Address:","Channel Name:","Audio Gain:","Group, Channel:","Device ID:","Frequency:","Transmitter Type:",
            "RF Power:","Transmitter Power Lock:","Transmitter Menu Lock:","Transmitter Mute:","Encryption:","Version "..PluginInfo.Version, "Encryption Toggle"}
  }

  local gaMeterLabels = {
    Pos = {X={380,470,380,470},Y={90,90,260,260}},
    Text = {"Audio Level","RF Level", "Antenna A", "Antenna B"} 
  }

  local gaLayouts = {
    Pos = {X={175,175,175,175,175,175,175,175,175,175,175,175,175},Y={90,110,130,150,170,190,210,230,250,270,290,310,330}},
    Text = {"UnitFirmware","UnitMacAddress","ChannelName","AudioGain","Group-Channel","DeviceID","Frequency","TransmitterType","RFPower","TransmitterPowerLock",
            "TransmitterMenuLock","TransmitterMute","Encryption","AudioLevel","RFLevel"},
    PrettyName = {gsCat1.."Firmware Version",gsCat1.."Mac Address",gsCat1.."Channel Name",gsCat1.."Audio Gain",gsCat1.."Group, Channel",gsCat1.."Device ID",gsCat1.."Frequency",
                  gsCat1.."Transmitter Type",gsCat1.."RF Power",gsCat1.."Transmitter Power Lock",gsCat1.."Transmitter Menu Lock",gsCat1.."Transmitter Mute",gsCat1.."Encyption"}, 
    FB = {true,true,false,false,false,true,true,true,true,true,true,true,true,},
    FillColor = {gaColor.LtGray,gaColor.LtGray,gaColor.White,gaColor.White,gaColor.White,gaColor.LtGray,gaColor.LtGray,gaColor.LtGray,gaColor.LtGray,gaColor.LtGray,gaColor.LtGray,gaColor.LtGray,gaColor.LtGray}
  }

  --Basic Labels
  for i=1,#gaLabels.Pos.X do
    table.insert(graphics,{
      Type = "Text",
      Text = gaLabels.Text[i],
      Position = { gaLabels.Pos.X[i], gaLabels.Pos.Y[i]},
      Color = gaColor.White,
      Size = {150,16},
      FontSize = 12,
      HTextAlign = "Right"
    })
  end

  --Meter Labels
  for i=1,#gaMeterLabels.Pos.X do
    table.insert(graphics,{
      Type = "Text",
      Text = gaMeterLabels.Text[i],
      Position = { gaMeterLabels.Pos.X[i], gaMeterLabels.Pos.Y[i]},
      Color = gaColor.White,
      Size = {70,16},
      FontSize = 12,
      HTextAlign = "Center"
    })
  end

  --Named Components Layouts
  for i=1,#gaLayouts.Pos.X do
    layout[gaLayouts.Text[i]] = {
      PrettyName = gaLayouts.PrettyName[i],
      Style = "Indicator",
      Position = { gaLayouts.Pos.X[i], gaLayouts.Pos.Y[i]},
      Size = {180,16},
      Color = gaLayouts.FillColor[i],
      FontSize = 14,
      HTextAlign = "Center",
      IsReadOnly = gaLayouts.FB[i]
    }
  end
  layout["AudioLevel"] = {
    PrettyName = gsCat1.."Audio Level",
    Style = "Meter",
    MeterStyle = "Level",
    ShowTextBox = true,
    Position = {400,110},
    Size = {36,128},
    Color = gaColor.Green,
  }
  layout["RFLevel"] = {
    PrettyName = gsCat1.."RF Level",
    Style = "Meter",
    MeterStyle = "Level",
    ShowTextBox = true,
    Position = {490,110},
    Size = {36,128},
    Color = {223,0,36},
  }
  layout["AntennaA"] = {
    PrettyName = gsCat1.."Antenna A Status",
    Style = "Led",
    Position = {410,280},
    Size = {16,16},
    Color = {255,0,0},
    UnlinkOffColor = false,
    Margin = 3,
    IsReadOnly = true,
  }
  layout["AntennaB"] = {
    PrettyName = gsCat1.."Antenna B Status",
    Style = "Led",
    Position = {500,280},
    Size = {16,16},
    Color = {255,0,0},
    UnlinkOffColor = false,
    Margin = 3,
    IsReadOnly = true,
  }
  layout["EncryptionModeToggle"] = {
    PrettyName = gsCat1.."Encryption Mode Toggle",
    Style = "Button",
    ButtonStyle = "Trigger",
    ButtonVisualStyle = "Flat",
    CornerRadius = 2,
    Margin = 1,
    Position = {380,330},
    Size = {36,16},
    Color = gaColor.Green,
    UnlinkOffColor = false,
    OffColor = gaColor.Green
  }
elseif CurrentPage == "Battery" then
    table.insert(graphics,{
      Type = "GroupBox",
      Fill = gaColor.Black,
      CornerRadius = 5,
      StrokeWidth = 1,
      Position = {5,5},
      Size = {550,370}
    })
    Logo = "--[[ #encode "logo.svg" ]]"
    table.insert(graphics,{
      Type = "Svg",
      Image = Logo,
      Position = {40,5},
      Size = {300,64},
      ZOrder = 1000
    })
    table.insert(graphics,{
      Type = "Text",
      Text = "QLX-D",
      Position = {340, 5},
      Color = gaColor.Green,
      Size = {150,64},
      FontSize = 40,
      Font = "Open Sans",
      FontStyle = "Extrabold",
      HTextAlign = "Left"
    })
    table.insert(graphics,{
      Type = "Text",
      Text = "©",
      Position = {475, 20},
      Color = gaColor.Green,
      Size = {20,20},
      FontSize = 16,
      Font = "Open Sans",
      FontStyle = "Extrabold",
      HTextAlign = "Left"
    })
    table.insert(graphics,{
      Type = "Header",
      Text = "Battery Info",
      Position = {15,70},
      Size = {530,11},
      Color = gaColor.Green,
      FontSize = 16,
      Font = "Roboto",
      FontStyle = "Bold",
      HTextAlign = "Center",
    })
    local gaLabels = {
      Pos = {X={20,20,20,20,20,20,400},Y={110,130,150,170,190,210,354}},
      Text = {"Battery Type:", "Battery Cycles:", "Battery Run Time:", "Battery Temperature:", "Battery Charge:", "Battery Health:","Version "..PluginInfo.Version}
    }
    local gaMeterLabels = {
      Pos = {X={380},Y={90}},
      Text = {"Battery Level"} 
    }
    local gaLayouts = {
      Pos = {X={175,175,175,175,175,175},Y={110,130,150,170,190,210}},
      Text = {"BatteryType","BatteryCycle","BatteryRunTime","BatteryTemp","BatteryCharge","BatteryHealth"},
      PrettyName = {gsCat2.."Type",gsCat2.."Cycles",gsCat2.."Run Time",gsCat2.."Temperature",gsCat2.."Charge",gsCat2.."Health"}, 
    }
    --Basic Labels
    for i=1,#gaLabels.Pos.X do
      table.insert(graphics,{
        Type = "Text",
        Text = gaLabels.Text[i],
        Position = { gaLabels.Pos.X[i], gaLabels.Pos.Y[i]},
        Color = gaColor.White,
        Size = {150,16},
        FontSize = 12,
        HTextAlign = "Right"
      })
    end
    --Meter Labels
    for i=1,#gaMeterLabels.Pos.X do
      table.insert(graphics,{
        Type = "Text",
        Text = gaMeterLabels.Text[i],
        Position = { gaMeterLabels.Pos.X[i], gaMeterLabels.Pos.Y[i]},
        Color = gaColor.White,
        Size = {100,16},
        FontSize = 12,
        HTextAlign = "Center"
      })
    end
    --Named Components Layouts
    for i=1,#gaLayouts.Pos.X do
      layout[gaLayouts.Text[i]] = {
        PrettyName = gaLayouts.PrettyName[i],
        Style = "Indicator",
        Position = { gaLayouts.Pos.X[i], gaLayouts.Pos.Y[i]},
        Size = {180,16},
        Color = gaColor.LtGray,
        FontSize = 14,
        HTextAlign = "Center",
        IsReadOnly = true
      }
    end
    layout["BatteryLevel"] = {
      PrettyName = gsCat2.."Level",
      Style = "Meter",
      MeterStyle = "Segmented",
      ShowTextBox = true,
      Position = {412,110},
      Size = {36,128},
      Color = gaColor.Green,
    }
elseif CurrentPage == "Setup" then
  table.insert(graphics,{
    Type = "GroupBox",
    Fill = gaColor.Black,
    CornerRadius = 5,
    StrokeWidth = 1,
    Position = {5,5},
    Size = {550,370}
  })
  Logo = "--[[ #encode "logo.svg" ]]"
  table.insert(graphics,{
    Type = "Svg",
    Image = Logo,
    Position = {40,5},
    Size = {300,64},
    ZOrder = 1000
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "QLX-D",
    Position = {340, 5},
    Color = gaColor.Green,
    Size = {150,64},
    FontSize = 40,
    Font = "Open Sans",
    FontStyle = "Extrabold",
    HTextAlign = "Left"
  })
  table.insert(graphics,{
    Type = "Text",
    Text = "©",
    Position = {475, 20},
    Color = gaColor.Green,
    Size = {20,20},
    FontSize = 16,
    Font = "Open Sans",
    FontStyle = "Extrabold",
    HTextAlign = "Left"
  })
  table.insert(graphics,{
    Type = "Header",
    Text = "Setup",
    Position = {15,70},
    Size = {530,11},
    Color = gaColor.Green,
    FontSize = 16,
    Font = "Roboto",
    FontStyle = "Bold",
    HTextAlign = "Center",
  })
  local gaLabels = {
    Pos = {X={30,30,400},Y={110,130,354}},
    Text = {"IP Address:","Connection Status:","Version "..PluginInfo.Version,}
  }
  --Basic Labels
  for i=1,#gaLabels.Pos.X do
    table.insert(graphics,{
      Type = "Text",
      Text = gaLabels.Text[i],
      Position = { gaLabels.Pos.X[i], gaLabels.Pos.Y[i]},
      Color = gaColor.White,
      Size = {150,16},
      FontSize = 12,
      HTextAlign = "Right"
    })
  end
  layout["IPAddress"] = {
    PrettyName = "Connection Info~Unit IP Address",
    Style = "Indicator",
    Position = {185,110},
    Size = {250,16},
    Color = gaColor.White,
    FontSize = 14,
    HTextAlign = "Center",
    IsReadOnly = false
  }
  
  layout["Status"] = {
    PrettyName = "Connection Info~Connection Status",
    Style = "Indicator",
    Position = {185,130},
    Size = {250,100},
    Color = {255,255,255},
    FontSize = 16,
    HTextAlign = "Center",
    IsReadOnly = true
  }
end