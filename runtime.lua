--TCP Socket Defined
------------------------------------------------------------
gsIPAddress = Controls["IPAddress"].String
gsPort = 2202

TCPClient = TcpSocket.New() --Define Socket
TCPClient.ReadTimeout = 0
TCPClient.WriteTimeout = 0
TCPClient.ReconnectTimeout = 5

--Debug Function
---------------------------------------------------------
function DebugFormat(string) -- Format strings containing non-printable characters so we can see what they are
  local visual = ""
  for i=1,#string do
    local byte = string:sub(i,i)
    if string.byte(byte) >= 32 and string.byte(byte) <= 126 then
      visual = visual..byte
    else
      visual = visual..string.format("[%02xh]",string.byte(byte))
    end
  end
  return visual
end

DebugTx = false
DebugRx = false
DebugFunction = false
DebugPrint = Properties["Debug Print"].Value

-- A function to determine common print statement scenarios for troubleshooting
function SetupDebugPrint()
  if DebugPrint=="Tx/Rx" then
    DebugTx,DebugRx=true,true
  elseif DebugPrint=="Tx" then
    DebugTx=true
  elseif DebugPrint=="Rx" then
    DebugRx=true
  elseif DebugPrint=="Function Calls" then
    DebugFunction=true
  elseif DebugPrint=="All" then
    DebugTx,DebugRx,DebugFunction=true,true,true
  end
end

SetupDebugPrint()

--Value Constants
-----------------------------------------------------------
gsFWVersion = "FW_VER"
gsChanName = "CHAN_NAME"
gsDeviceID = "DEVICE_ID"
gsAudioGain = "AUDIO_GAIN"
gsGroupChannel = "GROUP_CHAN"
gsFreq = "FREQUENCY"
gsBattType = "BATT_TYPE"
gsBattBars = "BATT_BARS"
gsTXType = "TX_TYPE"
gsTXRFPWR = "TX_RF_PWR"
gsTXPWRLock = "TX_PWR_LOCK"
gsTXMenuLock = "TX_MENU_LOCK"
gsEncryption = "ENCRYPTION " 
gsTXMuteStatus = "TX_MUTE_STATUS"
gsMACAddress = "MAC_ADDR"
gsBattCyle = "BATT_CYCLE"
gsBattRunTime = "BATT_RUN_TIME"
gsBattTemp = "BATT_TEMP_F"
gsBattCharge = "BATT_CHARGE"
gsBattHealth = "BATT_HEALTH"
gsArray = {gsFWVersion,gsChanName,gsDeviceID,gsAudioGain,gsGroupChannel,gsFreq,gsBattType,gsBattBars,gsTXType,gsTXRFPWR,gsTXPWRLock,gsTXMenuLock,gsEncryption,gsTXMuteStatus,gsMACAddress,gsBattCyle,gsBattRunTime,gsBattTemp,gsBattCharge,gsBattHealth}

gcFWVersion = Controls["UnitFirmware"]
gcChanName = Controls["ChannelName"]
gcDeviceID = Controls["DeviceID"]	
gcAudioGain = Controls["AudioGain"]	
gcGroupChannel = Controls["Group-Channel"]	
gcFreq = Controls["Frequency"]	
gcBattType = Controls["BatteryType"]
gcBattBars = Controls["BatteryLevel"]	
gcTXType = Controls["TransmitterType"]	
gcTXRFPWR = Controls["RFPower"]	
gcTXPWRLock = Controls["TransmitterPowerLock"]
gcTXMenuLock = Controls["TransmitterMenuLock"]
gcEncryption = Controls["Encryption"]
gcTXMuteStatus = Controls["TransmitterMute"]
gcMACAddress = Controls["UnitMacAddress"]
gcBattCyle = Controls["BatteryCycle"]
gcBattRunTime = Controls["BatteryRunTime"]
gcBattTemp = Controls["BatteryTemp"]
gcBattCharge = Controls["BatteryCharge"]
gcBattHealth = Controls["BatteryHealth"]
gcArray = {gcFWVersion,gcChanName,gcDeviceID,gcAudioGain,gcGroupChannel,gcFreq,gcBattType,gcBattBars,gcTXType,gcTXRFPWR,gcTXPWRLock,gcTXMenuLock,gcEncryption,gcTXMuteStatus,gcMACAddress,gcBattCyle,gcBattRunTime,gcBattTemp,gcBattCharge,gcBattHealth}

--Parse Data
-----------------------------------------------------------
function dataNotSupportedCheck(lsReturnVal, gaArrayIndex)
  if(gaArrayIndex == 16 )then  --For rechargable Battery cycles When using AA or unit is off
      if(lsReturnVal == "65535")then
        return("Off/Not Applicable")
      else
        return(lsReturnVal)
      end
  elseif(gaArrayIndex == 17 )then  --For rechargable Battery run time When using AA or unit is off
      if(lsReturnVal == "65535")then
        return("Off/Not Applicable")
      elseif(lsReturnVal == "65533")then 
        return("Charging")
      elseif(lsReturnVal == "65534")then 
        return("Calculating")
      else
        return(string.format("Time Left: %d:%02d", math.floor(lsReturnVal/60), math.fmod(lsReturnVal,60)))
      end
  elseif(gaArrayIndex == 18 )then  --For rechargable Battery temp When using AA or unit is off
    if(lsReturnVal == "255")then
      return("Off/Not Applicable")    
    else
      return(string.format((lsReturnVal - 40).." F"))
    end
  elseif(gaArrayIndex == 19 )then  --For rechargable Battery charge When using AA or unit is off
    if(lsReturnVal == "255")then
      return("Not Applicable")
    else
      return(string.format(lsReturnVal.."%"))
    end
  elseif(gaArrayIndex == 20 )then  --For rechargable Battery Health When using AA or unit is off
    if(lsReturnVal == "255")then
      return("Not Applicable")
    else
      return(string.format(lsReturnVal.."%"))
    end
  else
    return(lsReturnVal)
  end
end

function rxDataParse(lsRXData)
  if(DebugRx)then print(DebugFormat(lsRXData))end --Debug
  
  local lsCommandReturn = string.sub(lsRXData,3,5)
  
  if(lsCommandReturn == "REP")then  --Report Returned
    for x,reports in pairs(gsArray)do       --Search through All Reported Values
        if(string.find(lsRXData, gsArray[x]) ~= nil)then --If you find the value match and its at least one succesfully matach found
            local lsStart, lsEnd = string.find(lsRXData,gsArray[x]) --Find start and end position of Match
            local lsLen = string.len(lsRXData) -- Find total length of string
            
            if(x == 13)then --If Encryption Detected Filter Out Space
              lsEnd = lsEnd + 1
            else
              lsEnd = lsEnd + 2
            end
            
            local lsFilteredString = string.sub(lsRXData, lsEnd, (lsLen - 1)) --Pull out just raw value and send it to controls
          
          if(string.find(lsFilteredString, "{") or string.find(lsFilteredString, "}") ~= nil)then  --If you find brackets Cleanup
              lsFilteredString = string.gsub(lsFilteredString, "{", "")
              lsFilteredString = string.gsub(lsFilteredString, "}", "")              
              gcArray[x].String = dataNotSupportedCheck(lsFilteredString, x)
          else
            gcArray[x].String = dataNotSupportedCheck(lsFilteredString, x)
          end
        end
    end
  elseif(lsCommandReturn == "SAM")then  --Sample Levels Returned
    local lsAntennaInfo = string.sub(lsRXData, 16, 17)  --Antenna Infor
    Controls["RFLevel"].String = string.sub(lsRXData, 19, 21)  --RF Level
    Controls["AudioLevel"].String = string.sub(lsRXData, 23, 25)  --Audio Level
    
    if(lsAntennaInfo == "AX")then
      Controls["AntennaA"].Boolean = true
      Controls["AntennaB"].Boolean = false
    elseif(lsAntennaInfo == "XB")then
      Controls["AntennaA"].Boolean = false
      Controls["AntennaB"].Boolean = true
    elseif(lsAntennaInfo == "XX")then
      Controls["AntennaA"].Boolean = false
      Controls["AntennaB"].Boolean = false
    end
  end  

end


--Timers
-----------------------------------------------------------
QueryTimer = Timer.New()

QueryTimer.EventHandler = function()
  TCPClient:Write("< GET 1 ALL >")            ---Get All Data
    if(DebugTx)then print(DebugFormat("TX: ".."< GET 1 ALL >"))end
  TCPClient:Write("< GET ENCRYPTION >")
    if(DebugTx)then print(DebugFormat("TX: ".."< GET ENCRYPTION >"))end
end

--TCP Socket Event Handlers
-----------------------------------------------------------
TCPClient.Connected = function(TCPClient)     --Is Connected
  if(DebugFunction)then print("TCP socket is connected")end
  Controls["Status"].Value = 0
  Controls["Status"].String = '' 
  
  QueryTimer:Start(15)
  TCPClient:Write("< SET 1 METER_RATE 300 >")            ---Get Sample Data
    if(DebugTx)then print(DebugFormat("TX: ".."< SET 1 METER_RATE 300 >"))end
  TCPClient:Write("< GET 1 ALL >")            ---Get All Data Initially
    if(DebugTx)then print(DebugFormat("TX: ".."< GET 1 ALL >"))end
  TCPClient:Write("< GET ENCRYPTION >")
    if(DebugTx)then print(DebugFormat("TX: ".."< GET ENCRYPTION >"))end

end
TCPClient.Reconnect = function(TCPClient)     --Reconnecting
  if(DebugFunction)then print("TCP socket is reconnecting")end
  Controls["Status"].Value = 5
  Controls["Status"].String = 'Reconnecting'
end
TCPClient.Data = function(TCPClient)          --RX Data
  if(DebugFunction)then print("TCP socket has data:")end
  repeat
    msg = TCPClient:ReadLine(TcpSocket.EOL.Custom,">")
    if(msg ~= nil)then
      rxDataParse(msg)
      if(DebugRx)then print(DebugFormat("Raw Received Message: "..msg))end
    else
      break
    end
  until(TCPClient.BufferLength == 0)
end
TCPClient.Closed = function(TCPClient)        --Socket Closed
  if(DebugFunction)then print("TCP socket was closed by the remote end")end
  Controls["Status"].Value = 2 
  Controls["Status"].String = 'Closed'
  QueryTimer:Stop()
end
TCPClient.Error = function(TCPClient, Err)    --Error On Socket
  if(DebugFunction)then print("TCP socket had an error:", Err)end
  Controls["Status"].Value = 2
  Controls["Status"].String = Err
  QueryTimer:Stop()
end
TCPClient.Timeout = function(TCPClient, Err)   --Timeout Occured
  if(DebugFunction)then print("TCP socket timed out", Err)end
  Controls["Status"].Value = 2
  Controls["Status"].String = 'TimeOut'
  QueryTimer:Stop()
end
 
 
--Connect Events
---------------------------------------------------------- 
if(gsIPAddress ~= "" and gsPort ~= "")then      --Connect on Startup
  TCPClient:Connect(gsIPAddress, gsPort)
else
  Controls["Status"].Value = 2 
  Controls["Status"].String = 'Invalid IP Address'
end

Controls["IPAddress"].EventHandler = function()
    gsIPAddress = Controls["IPAddress"].String
    TCPClient:Disconnect()
    if(gsIPAddress ~= "" and gsPort ~= "")then 
      TCPClient:Connect(gsIPAddress, gsPort)
    else
      Controls["Status"].Value = 2 
      Controls["Status"].String = 'Invalid IP Address'
    end  
end


--Set Events
---------------------------------------------------------
Controls["ChannelName"].EventHandler = function()
  if(string.len(Controls["ChannelName"].String) <= 8)then
    TCPClient:Write("< SET 1 CHAN_NAME {"..Controls["ChannelName"].String.."} >")            --Write Channel Name 8 chr max
    if(DebugTx)then print(DebugFormat("TX: ".."< SET 1 CHAN_NAME {"..Controls["ChannelName"].String.."} >"))end
  else
  end
end

Controls["AudioGain"].EventHandler = function()
    TCPClient:Write("< SET 1 AUDIO_GAIN "..Controls["AudioGain"].String.." >")            --Write Channel Gain 000-060 values
    if(DebugTx)then print(DebugFormat("TX: ".."< SET 1 AUDIO_GAIN "..Controls["AudioGain"].String.." >"))end
end

Controls["Group-Channel"].EventHandler = function()
    TCPClient:Write("< SET 1 GROUP_CHAN "..Controls["Group-Channel"].String.." >")                                   --Write Group and Channel Need Commma Separate
    if(DebugTx)then print(DebugFormat("TX: ".."< SET 1 GROUP_CHAN "..Controls["Group-Channel"].String.." >"))end

end

--[[Controls["EncryptionModeToggle"].EventHandler = function()
  if(string.find(Controls["Encryption"].String, "ON") ~= nil)then
    TCPClient:Write("< SET ENCRYPTION OFF >")                                   --Toggle Encryption Off
  else
    TCPClient:Write("< SET ENCRYPTION ON >")                                   --Toggle Encryption On
  end
end]]--